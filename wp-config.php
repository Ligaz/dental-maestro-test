<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dental');

/** MySQL database username */
define('DB_USER', 'dental');

/** MySQL database password */
define('DB_PASSWORD', 'dental');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/p|uytqm04Ju87)T0J=rqs9fdck*oPIw$mABS:_q/y}tN>Ds5pdllb9Qm#1(#T_K');
define('SECURE_AUTH_KEY',  '9`YoI+hDi3:>Ga/~_Rb+C$m@s,yBXQ,(iZ~jZu+<JhpONYl4tLSp,-1UND5-?BNm');
define('LOGGED_IN_KEY',    '0f-X}$iGJIruOi7#S`_*S1N{5N(4~^;7(wj;csGAor$sceHRNBD^,%ktocC#&neU');
define('NONCE_KEY',        'XEJ}RVE$,(%w;:_KUtgl,~,in}mq~|D!/R]5`K~7#JO}GRj0^qR`!99w%m+6v>+G');
define('AUTH_SALT',        'L ;[B]g5VgFu`@1w^)vd]GrFdwf%W[Ziv5q}<|bt.r05Jj6k8.56deQtQvp5$hVN');
define('SECURE_AUTH_SALT', 'qF^@^15Y&z:-#vOf8OYNFjtaY%FODx-ZEaz/EaFnbf9JuBl@8L@,4DQ9Cw*ifsq0');
define('LOGGED_IN_SALT',   'P!ADtb//m.6,DP{s5UBDE`2pmQ&9Z@2<@QAjN@<f~&rjGz$m&3yOi<boEx:xVWX=');
define('NONCE_SALT',       'AF%ep3O8`OdJ#?)8Y<fYw[B(O^:ev?Tmd>I} ziY(7ik)CZ  9f!l[V;.Sh*k~73');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
