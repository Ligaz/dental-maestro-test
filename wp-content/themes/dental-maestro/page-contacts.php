<?php get_header(); ?>
    <!-- Begin wrapper -->
    <div class="wrapper">
<?php get_template_part('templates/breadcrumbs');?>
        <div class="container-fluid">
            <h1 class="title-bordered">Контакты</h1>

            <div class="contacts">
                <div class="contacts__description">
                    <h3>Учебный центр стоматологии</h3>
                    <p>г. Запорожье, ул. Базарная (Анголенко), 14-В.</p>
                    <p><a href="mailto:0676127717@dental-maestro.pro">0676127717@dental-maestro.pro</a></p>
                    <p>+38 067 612-77-17 (Viber);</p>
                    <p><a href="tel:+38 061 764-24">+38 061 764-24-89</a></p>
                </div>
                <div class="contacts__place">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2679.4887470751637!2d35.18406511605105!3d47.81074547919881!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40dc5e73db2fc395%3A0xb5a3f5b3f9fad537!2z0LLRg9C70LjRhtGPINCR0LDQt9Cw0YDQvdCwLCAxNCwg0JfQsNC_0L7RgNGW0LbQttGPLCDQl9Cw0L_QvtGA0ZbQt9GM0LrQsCDQvtCx0LvQsNGB0YLRjA!5e0!3m2!1sru!2sua!4v1491228988871" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>

    </div>
    <!-- End wrapper -->
<?php get_footer(); ?>