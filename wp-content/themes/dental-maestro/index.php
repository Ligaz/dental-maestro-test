<?php get_header() ?>
	<!-- Begin wrapper -->
	<div class="wrapper">
        <?php get_template_part('templates/slider_events');?>
        <?php get_template_part('templates/cources');?>
        <?php get_template_part('templates/about_us');?>
        <?php get_template_part('templates/lektors_for_index');?>
        <?php get_template_part('templates/privileges');?>
        <?php get_template_part('templates/map');?>
        <?php get_template_part('templates/partners');?>
	</div>
	<!-- End wrapper -->
<?php get_footer();?>
