<?php get_header(); ?>
    <!-- Begin wrapper -->
    <div class="wrapper">

        <?php get_template_part('templates/breadcrumbs');?>

        <div class="container-fluid">
            <!-- Begin lecturer info block -->
            <?php while (have_posts()) {the_post();?>

                <div class="lecturer-info">
                    <div class="lecturer-info__title" style="flex-direction: column; padding-top: 0">
                        <div style="width: 100%">
                            <img src="<?php echo get_field('lektor_photo')?>" >
                        </div>
                            <p><?php echo get_field('lektor_name'); ?></p>
                    </div>
                    <div class="lecturer-info__description">
                        <p><?php echo apply_filters('clip_string',get_field('lektor_description'),500); ?></p>
                    </div>
                </div>

            <?php } ?>
            <!-- End lecturer info block -->
        </div>

    </div>
    <!-- End wrapper -->
<?php get_footer();?>