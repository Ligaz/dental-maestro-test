"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function (document) {
	var _this = this;

	// Polyfills
	// classList
	;if ("document" in self && !("classList" in document.createElement("_"))) {
		(function (j) {
			"use strict";
			if (!("Element" in j)) {
				return;
			}var a = "classList",
			    f = "prototype",
			    m = j.Element[f],
			    b = Object,
			    k = String[f].trim || function () {
				return this.replace(/^\s+|\s+$/g, "");
			},
			    c = Array[f].indexOf || function (q) {
				var p = 0,
				    o = this.length;for (; p < o; p++) {
					if (p in this && this[p] === q) {
						return p;
					}
				}return -1;
			},
			    n = function n(o, p) {
				this.name = o;this.code = DOMException[o];this.message = p;
			},
			    g = function g(p, o) {
				if (o === "") {
					throw new n("SYNTAX_ERR", "An invalid or illegal string was specified");
				}if (/\s/.test(o)) {
					throw new n("INVALID_CHARACTER_ERR", "String contains an invalid character");
				}return c.call(p, o);
			},
			    d = function d(s) {
				var r = k.call(s.getAttribute("class") || ""),
				    q = r ? r.split(/\s+/) : [],
				    p = 0,
				    o = q.length;for (; p < o; p++) {
					this.push(q[p]);
				}this._updateClassName = function () {
					s.setAttribute("class", this.toString());
				};
			},
			    e = d[f] = [],
			    i = function i() {
				return new d(this);
			};n[f] = Error[f];e.item = function (o) {
				return this[o] || null;
			};e.contains = function (o) {
				o += "";return g(this, o) !== -1;
			};e.add = function () {
				var s = arguments,
				    r = 0,
				    p = s.length,
				    q,
				    o = false;do {
					q = s[r] + "";if (g(this, q) === -1) {
						this.push(q);o = true;
					}
				} while (++r < p);if (o) {
					this._updateClassName();
				}
			};e.remove = function () {
				var t = arguments,
				    s = 0,
				    p = t.length,
				    r,
				    o = false;do {
					r = t[s] + "";var q = g(this, r);if (q !== -1) {
						this.splice(q, 1);o = true;
					}
				} while (++s < p);if (o) {
					this._updateClassName();
				}
			};e.toggle = function (p, q) {
				p += "";var o = this.contains(p),
				    r = o ? q !== true && "remove" : q !== false && "add";if (r) {
					this[r](p);
				}return !o;
			};e.toString = function () {
				return this.join(" ");
			};if (b.defineProperty) {
				var l = { get: i, enumerable: true, configurable: true };try {
					b.defineProperty(m, a, l);
				} catch (h) {
					if (h.number === -2146823252) {
						l.enumerable = false;b.defineProperty(m, a, l);
					}
				}
			} else {
				if (b[f].__defineGetter__) {
					m.__defineGetter__(a, i);
				}
			}
		})(self);
	};

	// scrollIntoView smooth scroll
	!function (a, b, c) {
		"use strict";
		function d() {
			function h(a, b) {
				this.scrollLeft = a, this.scrollTop = b;
			}function i(a) {
				return .5 * (1 - Math.cos(Math.PI * a));
			}function j(a) {
				if ("object" != (typeof a === "undefined" ? "undefined" : _typeof(a)) || null === a || a.behavior === c || "auto" === a.behavior || "instant" === a.behavior) return !0;if ("object" == (typeof a === "undefined" ? "undefined" : _typeof(a)) && "smooth" === a.behavior) return !1;throw new TypeError("behavior not valid");
			}function k(c) {
				var d, e, f;do {
					c = c.parentNode, d = c === b.body, e = c.clientHeight < c.scrollHeight || c.clientWidth < c.scrollWidth, f = "visible" === a.getComputedStyle(c, null).overflow;
				} while (!d && (!e || f));return d = e = f = null, c;
			}function l(b) {
				b.frame = a.requestAnimationFrame(l.bind(a, b));var d,
				    f,
				    h,
				    c = g(),
				    j = (c - b.startTime) / e;if (j = j > 1 ? 1 : j, d = i(j), f = b.startX + (b.x - b.startX) * d, h = b.startY + (b.y - b.startY) * d, b.method.call(b.scrollable, f, h), f === b.x && h === b.y) return void a.cancelAnimationFrame(b.frame);
			}function m(c, d, e) {
				var i,
				    j,
				    k,
				    m,
				    o,
				    n = g();c === b.body ? (i = a, j = a.scrollX || a.pageXOffset, k = a.scrollY || a.pageYOffset, m = f.scroll) : (i = c, j = c.scrollLeft, k = c.scrollTop, m = h), o && a.cancelAnimationFrame(o), l({ scrollable: i, method: m, startTime: n, startX: j, startY: k, x: d, y: e, frame: o });
			}if (!("scrollBehavior" in b.documentElement.style)) {
				var d = a.HTMLElement || a.Element,
				    e = 468,
				    f = { scroll: a.scroll || a.scrollTo, scrollBy: a.scrollBy, scrollIntoView: d.prototype.scrollIntoView },
				    g = a.performance && a.performance.now ? a.performance.now.bind(a.performance) : Date.now;a.scroll = a.scrollTo = function () {
					return j(arguments[0]) ? void f.scroll.call(a, arguments[0].left || arguments[0], arguments[0].top || arguments[1]) : void m.call(a, b.body, ~~arguments[0].left, ~~arguments[0].top);
				}, a.scrollBy = function () {
					return j(arguments[0]) ? void f.scrollBy.call(a, arguments[0].left || arguments[0], arguments[0].top || arguments[1]) : void m.call(a, b.body, ~~arguments[0].left + (a.scrollX || a.pageXOffset), ~~arguments[0].top + (a.scrollY || a.pageYOffset));
				}, d.prototype.scrollIntoView = function () {
					if (j(arguments[0])) return void f.scrollIntoView.call(this, arguments[0] || !0);var c = k(this),
					    d = c.getBoundingClientRect(),
					    e = this.getBoundingClientRect();c !== b.body ? (m.call(this, c, c.scrollLeft + e.left - d.left, c.scrollTop + e.top - d.top), a.scrollBy({ left: d.left, top: d.top, behavior: "smooth" })) : a.scrollBy({ left: e.left, top: e.top, behavior: "smooth" });
				};
			}
		}"object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? module.exports = { polyfill: d } : d();
	}(window, document);

	// matches
	Element.prototype.matches || (Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector || function (e) {
		for (var t = (this.document || this.ownerDocument).querySelectorAll(e), o = t.length; --o >= 0 && t.item(o) !== this;) {}return o > -1;
	});

	// closest
	!function (t) {
		t.closest = t.closest || function (t) {
			for (var e = this; e;) {
				if (e.matches(t)) return e;e = e.parentElement;
			}return null;
		};
	}(Element.prototype);

	// textContent
	void 0 === document.documentElement.textContent && Object.defineProperty(HTMLElement.prototype, "textContent", { get: function get() {
			return this.innerText;
		}, set: function set(t) {
			this.innerText = t;
		} });

	// append
	!function (a) {
		a.forEach(function (a) {
			a.append = a.append || function () {
				var a = Array.prototype.slice.call(arguments),
				    b = document.createDocumentFragment();a.forEach(function (a) {
					var c = a instanceof Node;b.appendChild(c ? a : document.createTextNode(String(a)));
				}), this.appendChild(b);
			};
		});
	}([Element.prototype, Document.prototype, DocumentFragment.prototype]);

	// prepend
	!function (a) {
		a.forEach(function (a) {
			a.prepend = a.prepend || function () {
				var a = Array.prototype.slice.call(arguments),
				    b = document.createDocumentFragment();a.forEach(function (a) {
					var c = a instanceof Node;b.appendChild(c ? a : document.createTextNode(String(a)));
				}), this.insertBefore(b, this.firstChild);
			};
		});
	}([Element.prototype, Document.prototype, DocumentFragment.prototype]);

	// before
	!function (a) {
		a.forEach(function (a) {
			a.before = a.before || function () {
				var a = Array.prototype.slice.call(arguments),
				    b = document.createDocumentFragment();a.forEach(function (a) {
					var c = a instanceof Node;b.appendChild(c ? a : document.createTextNode(String(a)));
				}), this.parentNode.insertBefore(b, this);
			};
		});
	}([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

	// after
	!function (a) {
		a.forEach(function (a) {
			a.after = a.after || function () {
				var a = Array.prototype.slice.call(arguments),
				    b = document.createDocumentFragment();a.forEach(function (a) {
					var c = a instanceof Node;b.appendChild(c ? a : document.createTextNode(String(a)));
				}), this.parentNode.insertBefore(b, this.nextSibling);
			};
		});
	}([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

	// replaceWith
	!function (a) {
		a.forEach(function (a) {
			a.replaceWith = a.replaceWith || function () {
				var a = Array.prototype.slice.call(arguments),
				    b = document.createDocumentFragment();a.forEach(function (a) {
					var c = a instanceof Node;b.appendChild(c ? a : document.createTextNode(String(a)));
				}), this.parentNode.replaceChild(b, this);
			};
		});
	}([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

	// remove
	Element.prototype.remove || (Element.prototype.remove = function () {
		this.parentNode && this.parentNode.removeChild(this);
	});

	/*-------------------------------- Start --------------------------------*/
	// Modal window

	var Modal = function Modal(options) {
		_classCallCheck(this, Modal);

		this.elem = document.querySelector(options.elem + ' .modal');
		this.modalOverlay = document.querySelector(options.elem + ' .modal-overlay');
		this.closeButton = document.querySelector(options.elem + ' .close-modal');
		this.scrollWidth = window.innerWidth - document.documentElement.clientWidth;
	};

	Modal.prototype.open = function () {
		_this.elem.classList.add('opened');
		_this.modalOverlay.classList.add('opened');
		document.body.classList.add('opened-modal');
		document.body.style.paddingRight = _this.scrollWidth + 'px';
	};

	Modal.prototype.close = function () {
		_this.elem.classList.remove('opened');
		_this.modalOverlay.classList.remove('opened');
		document.body.classList.remove('opened-modal');
		document.body.style.paddingRight = 0;
	};

	var modal = new Modal({
		elem: '.modal-wrap'
	});

	function fadeIn(elem) {
		elem.style.display = 'block';
		setTimeout(function () {
			elem.style.opacity = 1;
		}, 1);
	}
	function fadeOut(elem, duration) {
		elem.style.opacity = 0;
		setTimeout(function () {
			elem.style.display = 'none';
		}, duration);
	}

	// set equal height blocks
	function setEqualHeight(columns, autoHeight) {
		var tallest = 0;
		if (autoHeight === true) {
			for (var i = 0; i < columns.length; ++i) {
				columns[i].style.height = 'auto';
			}
			return false;
		}

		for (var _i = 0; _i < columns.length; ++_i) {
			columns[_i].style.height = '';
			var currentHeight = columns[_i].clientHeight;
			if (currentHeight > tallest) {
				tallest = currentHeight;
			}
		}

		for (var _i2 = 0; _i2 < columns.length; ++_i2) {
			columns[_i2].style.height = tallest + 'px';
		}
	}

	// set padding-top to wrapper according to header height
	function setPaddingWrapper() {
		document.querySelector('.wrapper').style.paddingTop = document.querySelector('.header').clientHeight + 'px';
	}
	setPaddingWrapper();
	window.addEventListener('resize', setPaddingWrapper);

	// set negative margin to slider according to header height
	function marginTopSlider() {
		document.querySelector('.main-slider').style.marginTop = -document.querySelector('.header').clientHeight + 'px';
	}

	// main slider
	if (document.querySelector('.main-slider') !== null) {
		marginTopSlider();
		window.addEventListener('resize', marginTopSlider);

		var mainSlider = new Swiper('.main-slider__container', {
			speed: 400,
			loop: true,
			simulateTouch: false,
			nextButton: '.main-slider__next',
			prevButton: '.main-slider__prev',
			pagination: '.main-slider__pagination'
		});
	}

	// lecturers slider
	if (document.querySelector('.lecturers-slider') !== null) {
		var lecturersSlider = new Swiper('.lecturers-slider__container', {
			speed: 400,
			loop: true,
			loopedSlides: document.querySelectorAll('.photo-slider__container .swiper-wrapper .swiper-slide').length,
			simulateTouch: false,
			nextButton: '.lecturers-slider__next',
			prevButton: '.lecturers-slider__prev'
		});
	}

	// photo slider
	if (document.querySelector('.photo-slider') !== null) {
		var photoSlider = new Swiper('.photo-slider__container', {
			direction: 'horizontal',
			loop: true,
			slidesPerView: 'auto',
			simulateTouch: false,
			nextButton: '.photo-slider__next',
			prevButton: '.photo-slider__prev'
		});

		var photoSliderThumbs = new Swiper('.photo-slider__thumbnails-container', {
			spaceBetween: 12,
			centeredSlides: true,
			slidesPerView: 6,
			touchRatio: 0.2,
			slideToClickedSlide: true,
			nextButton: '.photo-slider__thumbnails-next',
			prevButton: '.photo-slider__thumbnails-prev',
			loop: true,
			loopedSlides: document.querySelectorAll('.photo-slider__container .swiper-slide').length,
			breakpoints: {
				767: {
					slidesPerView: 3,
					spaceBetween: 5
				},
				400: {
					slidesPerView: 2
				}
			}
		});

		photoSlider.params.control = photoSliderThumbs;
		photoSliderThumbs.params.control = photoSlider;
	}

	// Menu
	var menu = document.querySelectorAll('.main-menu');
	var menuButton = document.querySelector('.menu-button');
	menuButton.addEventListener('click', function (e) {
		if (!this.classList.contains('open')) {
			this.classList.add('open');
			for (var i = 0; i < menu.length; ++i) {
				fadeIn(menu[i]);
			}
		} else {
			this.classList.remove('open');
			for (var _i3 = 0; _i3 < menu.length; ++_i3) {
				fadeOut(menu[_i3], 500);
			}
		}
	});

	// hide menu by click body
	document.body.addEventListener('click', function (e) {
		if (!e.target.closest('.header__in')) {
			if (menuButton.classList.contains('open')) {
				for (var i = 0; i < menu.length; ++i) {
					fadeOut(menu[i], 500);
				}
				menuButton.classList.remove('open');
			}
		}
	});

	// set equal height to courses blocks
	function coursesHeight() {
		if (window.innerWidth > 850) setEqualHeight(document.querySelectorAll('.course__in-hover'));else setEqualHeight(document.querySelectorAll('.course__in-hover'), true);

		var courses = document.querySelectorAll('.course');
		for (var i = 0; i < courses.length; ++i) {
			courses[i].style.height = courses[i].querySelector('.course__in-hover').clientHeight + 'px';
		}
	}

	if (document.querySelector('.course') !== null) {
		coursesHeight();
		window.addEventListener('resize', coursesHeight);
	}

	// object-fit to img
	if (document.querySelector('.img-fit img') !== null) {
		var someImages = document.querySelectorAll('.img-fit img');
		objectFitImages(someImages);
	}

	if (document.querySelector('.js-button-form') !== null) {
		document.querySelector('.js-button-form').addEventListener('click', function () {
			document.querySelector('.form').classList.toggle('hidden');
		});
		document.querySelector('.form__close').addEventListener('click', function () {
			document.querySelector('.form').classList.add('hidden');
		});
	}

	// disable dragstart on images and links
	var images = document.getElementsByTagName('img');
	var hrefs = document.getElementsByTagName('a');
	for (var i = 0, img; img = images[i++];) {
		img.ondragstart = function () {
			return false;
		};
	}
	for (var _i4 = 0, href; href = hrefs[_i4++];) {
		href.ondragstart = function () {
			return false;
		};
	}
})(document);