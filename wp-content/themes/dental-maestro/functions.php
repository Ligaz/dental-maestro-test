<?php
//Добавляем файл стилей

function styles_scripts(){
    wp_enqueue_style( 'style', get_stylesheet_uri());
}
add_action( 'wp_enqueue_scripts', 'styles_scripts' );
//Добавляем кастомные поля для админки
//Курсы
function cources_columns($columns)
{
    $columns = array(
        'cb'         => '<input type="checkbox" />',
        'title'     => 'Название курса',
        'full_title'     => 'Полное название курса',
        'lektors' => 'Лекторы',
        'number_listeners'=>'Количество участников',
        'duration'=>'Продолжительность в днях',
        'directions'=>'Направление',
        'course_image'=>'Фото курса'
    );
    return $columns;
}

function handler_cources_columns($column)
{
    global $post;

    switch ($column){
        case 'full_title':
            echo get_field( "full_title", $post->ID );
            break;
        case 'lektors':
            $lektors = get_field('lektors',$post->ID );
            foreach($lektors as $lektor){
                $url = get_field( "lektor_photo", $lektor->ID);
                echo "<img src='$url' width='20px' height='20px'/><p>$lektor->post_title</p>";
            }
            break;
        case 'number_listeners':
            echo get_field( "number_listeners", $post->ID );
            break;
        case 'duration':
            echo get_field( "duration", $post->ID );
            break;
        case 'directions':
            $term = wp_get_post_terms($post->ID ,'directions');
            echo $term[0]->name;
            break;
        case 'course_image':
            $url = get_field( "course_image", $post->ID );
            echo "<img src='$url' width='50px' height='50px'/>";
            break;

    }
}
add_action("manage_cources_posts_custom_column", "handler_cources_columns");
add_filter("manage_cources_posts_columns", "cources_columns");
//Направления
function directions_columns($columns)
{
    $columns = array(
        'cb'         => '<input type="checkbox" />',
        'name'     => 'Название направления',
        'description' => 'Описание направления',
        'direct_title'     => 'Заголовок для курсов',
        'back_image'=>'Фоновое изображение',
        'icon'=>'Иконка'

    );
    return $columns;
}
function handler_directions_columns($out,$col_name,$id)
{
    $tax_name = 'directions';
    $direct = get_term($id, $tax_name);
    switch ($col_name){
        case 'name':
            echo $direct->name;
            break;
        case 'direct_title':
            echo get_field($col_name,$tax_name.'_'.$id);
            break;
        case 'back_image':
            $url = get_field($col_name,$tax_name.'_'.$id);
            echo "<img src='$url' width='50px' height='50px'/>";
            break;
        case 'icon':
            $url = get_field($col_name,$tax_name.'_'.$id);
            echo "<div style=' height: 50px; border-radius: 3px; width:50px; text-align:center; background: #000000'><img style='margin-top: 5px;' src='$url' width='40px' height='40px'/></div>";
            break;
    }
}
add_filter("manage_directions_custom_column", "handler_directions_columns",10,3);
add_filter("manage_edit-directions_columns", "directions_columns");
//Лекторы
function lektors_columns($columns)
{
    $columns = array(
        'cb'         => '<input type="checkbox" />',
        'title'     => 'Имя лектора',
        'lektor_photo' => 'Фото лектора',
        'lektor_description'     => 'Информация о докторе'

    );
    return $columns;
}
function handler_lektors_columns($column)
{
    global $post;

    switch ($column){
        case 'lektor_description':
            echo get_field( "lektor_description", $post->ID );
            break;
        case 'lektor_photo':
            $url = get_field( "lektor_photo", $post->ID );
            echo "<img src='$url' width='50px' height='50px'/>";
            break;
    }
}
add_action("manage_lektors_posts_custom_column", "handler_lektors_columns");
add_filter("manage_lektors_posts_columns", "lektors_columns");
//Мероприятия
function events_columns($columns)
{
    $columns = array(
        'cb'         => '<input type="checkbox" />',
        'title'     => 'Название мероприятия',
        'description' => 'Описание мероприятия',
        'date_events'     => 'Дата проведения',
        'photo_event'=> 'Фотография мероприятия'
    );
    return $columns;
}
function handler_events_columns($column)
{
    global $post;

    switch ($column){
        case 'description':
            echo get_field( "description", $post->ID );
            break;
        case 'date_events':
            echo date_formater(get_field( "date_events", $post->ID ));
            break;
        case 'photo_event':
            $url = get_field( "photo_event", $post->ID );
            echo "<img src='$url' width='50px' height='50px'/>";
            break;
    }
}
add_action("manage_events_posts_custom_column", "handler_events_columns");
add_filter("manage_events_posts_columns", "events_columns");
//Фото
function photo_columns($columns)
{
    $columns = array(
        'cb'         => '<input type="checkbox" />',
        'description' => 'Описание фото',
        'image'     => 'Изображение'
    );
    return $columns;
}
function handler_photo_columns($column)
{
    global $post;

    switch ($column){
        case 'description':
            echo get_field( "description", $post->ID );
            break;
        case 'image':
            $url = get_field( "image", $post->ID );
            echo "<img src='$url' width='300px' style='border-radius: 10px;'/>";
            break;
    }
}
add_action("manage_images_posts_custom_column", "handler_photo_columns");
add_filter("manage_images_posts_columns", "photo_columns");
//Галереи
function photogallery_columns($columns)
{
    $columns = array(
        'cb'         => '<input type="checkbox" />',
        'name'     => 'Название галереи',
        'description' => 'Краткое описание галереи',
        'gal_img'     => 'Обложка галереи'

    );
    return $columns;
}
function handler_photogallery_columns($out,$col_name,$id)
{
    $tax_name = 'photogallery';
    $gal = get_term($id, $tax_name);
    switch ($col_name){
        case 'name':
            echo $gal->name;
            break;
        case 'description':
            echo $gal->description;
            break;
        case 'gal_img':
            $url = get_field($col_name,$tax_name.'_'.$id);
            echo "<img src='$url' width='50px' height='50px'/>";
            break;
    }
}
add_action("manage_photogallery_custom_column", "handler_photogallery_columns",10,3);
add_filter("manage_edit-photogallery_columns", "photogallery_columns");
//Партнеры
function partners_columns($columns)
{
    $columns = array(
        'cb'         => '<input type="checkbox" />',
        'title'     => 'Название организации-партнера',
        'url' => 'Адрес сайта партнера',
        'img_photo' => 'Изображение'
    );
    return $columns;
}
function handler_partners_columns($column)
{
    global $post;

    switch ($column){
        case 'url':
            echo get_field( "url", $post->ID );
            break;
        case 'img_photo':
            $url = get_field( "img_photo", $post->ID );
            echo "<img src='$url' width='150px'/>";
            break;
    }
}
add_action("manage_partners_posts_custom_column", "handler_partners_columns");
add_filter("manage_partners_posts_columns", "partners_columns");
//Добавляем сортировку полей в админке
function sort_lektors( $columns ) {
    $columns['lektor_name'] = 'lektor_name';
    $columns['lektor_description'] = 'description';
    return $columns;
}
function sort_cources( $columns ) {
    $columns['full_title'] = 'full_title';
    $columns['duration'] = 'duration';
    return $columns;
}
function sort_gallery( $columns ) {
    $columns['description'] = 'description';
    return $columns;
}
add_filter( 'manage_edit-lektors_sortable_columns', 'sort_lektors' );
add_filter( 'manage_edit-cources_sortable_columns', 'sort_cources' );
add_filter( 'manage_edit-photogallery_sortable_columns', 'sort_gallery' );
//Добавляем custom placeholder для title в админке
function change_title($title){
    $screen = get_current_screen();
    $type = $screen->post_type;
    switch ($type){
        case 'lektors':
            return 'Ввведите имя лектора';
            break;

        case 'cources':
            return 'Введите название курса';
            break;
        case 'events':
            return 'Введите название мероприятия';
            break;
        case 'photogallery':
            return 'Введите название фото';
            break;
        case 'partners':
            return 'Введите название партнера';
            break;
        default:
            return $title;
    }

}
add_filter( 'enter_title_here', 'change_title' );
//Добавляем выборку по полям для админки
function add_filters(){
    product_type_filter('cources','directions');
    product_type_filter('images','photogallery');
}

function product_type_filter($type,$tax) {
    global $typenow;
    $post_type = $type;
    $taxonomy = $tax;
    if ($typenow == $post_type) {
        $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
        $info_taxonomy = get_taxonomy($taxonomy);
        wp_dropdown_categories(array(
            'show_option_all' => __("Все {$info_taxonomy->label}"),
            'taxonomy' => $taxonomy,
            'name' => $taxonomy,
            'orderby' => 'name',
            'selected' => $selected,
            'value_field' => 'slug',
            'show_count' => true,
            'hide_empty' => true,
        ));
    };
}
add_action('restrict_manage_posts', 'add_filters');
//Изменяем url согласно таксономии для пагинации
add_filter('post_link', 'modify_permalink', 10, 2);
add_filter('post_type_link', 'modify_permalink', 10, 2);
function modify_permalink($url, $post)
{
    if ($post->post_type != 'cources') {
        return $url;
    }
    $type = get_post_type_object($post->post_type);
    $term = get_the_terms($post->ID, 'directions');
    if ($term && count($term)) {
        $term = array_pop($term);
        $url = site_url('/') . ($type->rewrite ? $type->rewrite['slug'] . '/' : '') . $term->slug . '/' . $post->post_name;
        return $url;
    }
}
add_filter('request', 'true_post_type_request', 1, 1 );
//Меняем линки для таксоноимй
function true_post_type_request( $query ){
    global $wpdb;
    $post_type_name = 'cources';
    $tax_name = 'directions';
    $yarlik = $query['attachment'];
    $post_id = $wpdb->get_var(
        "
		SELECT ID
		FROM $wpdb->posts
		WHERE post_name = '$yarlik'
		AND post_type = '$post_type_name'
		"
    );
    $termini = wp_get_object_terms( $post_id, $tax_name );
    if( isset( $yarlik ) && $post_id && !is_wp_error( $termini ) && !empty( $termini ) ) :
        unset( $query['attachment'] );
        $query[$post_type_name] = $yarlik;
        $query['post_type'] = $post_type_name;
        $query['name'] = $yarlik;
    endif;
    return $query; // возвращаем результат
}
//Фильтры для view
//Фильтр дат в формат - "1 Мая 2017"
function date_formater($string){
    $date = date("d M Y", strtotime($string));
    $replace_month = array(
        'Jan'=>'Января',
        'Feb'=>'Февраля',
        'Mar'=>'Марта',
        'Apr'=>'Апреля',
        'May'=>'Мая',
        'Jun'=>'Июня',
        'Jul'=>'Июля',
        'Aug'=>'Августа',
        'Sep'=>'Сентября',
        'Oct'=>'Октября',
        'Nov'=>'Ноября',
        'Dec'=>'Декабря',
    );
    $replace_date = array(
        '01'=>'1',
        '02'=>'2',
        '03'=>'3',
        '04'=>'4',
        '05'=>'5',
        '06'=>'6',
        '07'=>'7',
        '08'=>'8',
        '09'=>'9'
    );
    $repace_year = array(
        '216'=>'2016',
        '217'=>'2017',
        '218'=>'2018',
        '219'=>'2019',
        '220'=>'2020',
        '221'=>'2021',
        '222'=>'2022',
        '223'=>'2023',
        '224'=>'2024'
    );
    $repl_m=  strtr($date, $replace_month);
    $repl_d = strtr($repl_m,$replace_date);
    return strtr($repl_d,$repace_year);
}
//Фильтр для кол-ва дней
add_filter('rus_date','date_formater');
function day_duration_formater($count){
    $count = abs($count) % 100;
    $lcount = $count % 10;
    if ($count >= 11 && $count <= 19) return 'дней';
    if ($lcount >= 2 && $lcount <= 4) return 'дня';
    if ($lcount == 1) return 'день';
    return 'дней';
}
add_filter('count_days','day_duration_formater');
//Фильтр для обрезки описаний
function clip_block($string,$count){
    $string = strip_tags($string);
    $entCount = iconv_strlen($string);
    if($count>$entCount){
        return $string;
    }else{
        $string = substr($string, 0, $count);
        $string = rtrim($string, "!,.-");
        $string = substr($string, 0, strrpos($string, ' '));
        return $string."… ";
    }

}
add_filter('clip_string','clip_block',10,2);
//Добавляем миниатюры
add_theme_support( 'post-thumbnails' );
//Регистрация меню
add_theme_support( 'menus' );
register_nav_menus(array('menu-left'=>'левая часть меню','menu-right'=>'правая часть меню'));
/**
 * Модифицированные хлебные крошки
 */
function kama_breadcrumbs( $sep = '', $l10n = array(), $args = array() ){
    $kb = new Kama_Breadcrumbs;
    echo $kb->get_crumbs( $sep, $l10n, $args );
}
class Kama_Breadcrumbs {

    public $arg;

    // Локализация
    static $l10n = array(
        'home'       => '',
        'paged'      => 'Страница %d',
        '_404'       => 'Ошибка 404',
        'search'     => 'Результаты поиска по запросу - <b>%s</b>',
        'author'     => 'Архив автора: <b>%s</b>',
        'year'       => 'Архив за <b>%d</b> год',
        'month'      => 'Архив за: <b>%s</b>',
        'day'        => '',
        'attachment' => 'Медиа: %s',
        'tag'        => 'Записи по метке: <b>%s</b>',
        'tax_tag'    => '%1$s из "%2$s" по тегу: <b>%3$s</b>',
        // tax_tag выведет: 'тип_записи из "название_таксы" по тегу: имя_термина'.
        // Если нужны отдельные холдеры, например только имя термина, пишем так: 'записи по тегу: %3$s'
    );

    // Параметры по умолчанию
    static $args = array(
        'on_front_page'   => true,  // выводить крошки на главной странице
        'show_post_title' => true,  // показывать ли название записи в конце (последний элемент). Для записей, страниц, вложений
        'show_term_title' => true,  // показывать ли название элемента таксономии в конце (последний элемент). Для меток, рубрик и других такс
        'title_patt'      => '<span class="current">%s</span>', // шаблон для последнего заголовка. Если включено: show_post_title или show_term_title
        'last_sep'        => true,  // показывать последний разделитель, когда заголовок в конце не отображается
        'markup'          => 'schema.org', // 'markup' - микроразметка. Может быть: 'rdf.data-vocabulary.org', 'schema.org', '' - без микроразметки
        // или можно указать свой массив разметки:
        // array( 'wrappatt'=>'<div class="kama_breadcrumbs">%s</div>', 'linkpatt'=>'<a href="%s">%s</a>', 'sep_after'=>'', )
        'priority_tax'    => array('category'), // приоритетные таксономии, нужно когда запись в нескольких таксах
        'priority_terms'  => array(), // 'priority_terms' - приоритетные элементы таксономий, когда запись находится в нескольких элементах одной таксы одновременно.
        // Например: array( 'category'=>array(45,'term_name'), 'tax_name'=>array(1,2,'name') )
        // 'category' - такса для которой указываются приор. элементы: 45 - ID термина и 'term_name' - ярлык.
        // порядок 45 и 'term_name' имеет значение: чем раньше тем важнее. Все указанные термины важнее неуказанных...
        'nofollow' => false, // добавлять rel=nofollow к ссылкам?

        // служебные
        'sep'             => '',
        'linkpatt'        => '',
        'pg_end'          => '',
    );

    function get_crumbs( $sep, $l10n, $args ){
        global $post, $wp_query, $wp_post_types;

        self::$args['sep'] = $sep;

        // Фильтрует дефолты и сливает
        $loc = (object) array_merge( apply_filters('kama_breadcrumbs_default_loc', self::$l10n ), $l10n );
        $arg = (object) array_merge( apply_filters('kama_breadcrumbs_default_args', self::$args ), $args );

        $arg->sep = '<span class="divider">'. $arg->sep .'</span>'; // дополним

        // упростим
        $sep = & $arg->sep;
        $this->arg = & $arg;

        // микроразметка ---
        if(1){
            $mark = & $arg->markup;

            // Разметка по умолчанию
            if( ! $mark ) $mark = array(
                'wrappatt'  => '<div class="">%s</div>',
                'linkpatt'  => '<a href="%s">%s</a>',
                'sep_after' => '',
            );
            // rdf
            elseif( $mark === 'rdf.data-vocabulary.org' ) $mark = array(
                'wrappatt'   => '<div class="" prefix="v: http://rdf.data-vocabulary.org/#">%s</div>',
                'linkpatt'   => '<span typeof="v:Breadcrumb"><a href="%s" rel="v:url" property="v:title">%s</a>',
                'sep_after'  => '</span>', // закрываем span после разделителя!
            );
            // schema.org
            elseif( $mark === 'schema.org' ) $mark = array(
                'wrappatt'   => '<div class="" itemscope itemtype="http://schema.org/BreadcrumbList">%s</div>',
                'linkpatt'   => '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="%s" itemprop="item"><span itemprop="name">%s</span></a><meta itemprop="position" content="1"> </span>',
                'sep_after'  => '',
            );

            elseif( ! is_array($mark) )
                die( __CLASS__ .': "markup" parameter must be array...');

            $wrappatt  = $mark['wrappatt'];
            $arg->linkpatt  = $arg->nofollow ? str_replace('<a ','<a rel="nofollow"', $mark['linkpatt']) : $mark['linkpatt'];
            $arg->sep      .= $mark['sep_after']."\n";
        }

        $linkpatt = $arg->linkpatt; // упростим

        $q_obj = get_queried_object();

        // может это архив пустой таксы?
        $ptype = null;
        if( empty($post) ){
            if( isset($q_obj->taxonomy) )
                $ptype = & $wp_post_types[ get_taxonomy($q_obj->taxonomy)->object_type[0] ];
        }
        else $ptype = & $wp_post_types[ $post->post_type ];

        // paged
        $arg->pg_end = '';
        if( ($paged_num = get_query_var('paged')) || ($paged_num = get_query_var('page')) )
//            $arg->pg_end = $sep . sprintf( $loc->paged, (int) $paged_num );
//
        $pg_end = $arg->pg_end; // упростим

        // ну, с богом...
        $out = '';

        if( is_front_page() ){
            return $arg->on_front_page ? sprintf( $wrappatt, ( $paged_num ? sprintf($linkpatt, get_home_url(), $loc->home) . $pg_end : $loc->home ) ) : '';
        }
        // страница записей, когда для главной установлена отдельная страница.
        elseif( is_home() ) {
            $out = $paged_num ? ( sprintf( $linkpatt, get_permalink($q_obj), esc_html($q_obj->post_title) ) . $pg_end ) : esc_html($q_obj->post_title);
        }
        elseif( is_404() ){
            $out = $loc->_404;
        }
        elseif( is_search() ){
            $out = sprintf( $loc->search, esc_html( $GLOBALS['s'] ) );
        }
        elseif( is_author() ){
            $tit = sprintf( $loc->author, esc_html($q_obj->display_name) );
            $out = ( $paged_num ? sprintf( $linkpatt, get_author_posts_url( $q_obj->ID, $q_obj->user_nicename ) . $pg_end, $tit ) : $tit );
        }
        elseif( is_year() || is_month() || is_day() ){
            $y_url  = get_year_link( $year = get_the_time('Y') );

            if( is_year() ){
                $tit = sprintf( $loc->year, $year );
                $out = ( $paged_num ? sprintf($linkpatt, $y_url, $tit) . $pg_end : $tit );
            }
            // month day
            else {
                $y_link = sprintf( $linkpatt, $y_url, $year);
                $m_url  = get_month_link( $year, get_the_time('m') );

                if( is_month() ){
                    $tit = sprintf( $loc->month, get_the_time('F') );
                    $out = $y_link . $sep . ( $paged_num ? sprintf( $linkpatt, $m_url, $tit ) . $pg_end : $tit );
                }
                elseif( is_day() ){
                    $m_link = sprintf( $linkpatt, $m_url, get_the_time('F'));
                    $out = $y_link . $sep . $m_link . $sep . get_the_time('l');
                }
            }
        }
        // Древовидные записи
        elseif( is_singular() && $ptype->hierarchical ){
            $out = $this->_add_title( $this->_page_crumbs($post), $post );
        }
        // Таксы, плоские записи и вложения
        else {
            $term = $q_obj; // таксономии

            // определяем термин для записей (включая вложения attachments)
            if( is_singular() ){
                // изменим $post, чтобы определить термин родителя вложения
                if( is_attachment() && $post->post_parent ){
                    $save_post = $post; // сохраним
                    $post = get_post($post->post_parent);
                }

                // учитывает если вложения прикрепляются к таксам древовидным - все бывает :)
                $taxonomies = get_object_taxonomies( $post->post_type );
                // оставим только древовидные и публичные, мало ли...
                $taxonomies = array_intersect( $taxonomies, get_taxonomies( array('hierarchical' => true, 'public' => true) ) );

                if( $taxonomies ){
                    // сортируем по приоритету
                    if( ! empty($arg->priority_tax) ){
                        usort( $taxonomies, function($a,$b)use($arg){
                            $a_index = array_search($a, $arg->priority_tax);
                            if( $a_index === false ) $a_index = 9999999;

                            $b_index = array_search($b, $arg->priority_tax);
                            if( $b_index === false ) $b_index = 9999999;

                            return ( $b_index === $a_index ) ? 0 : ( $b_index < $a_index ? 1 : -1 ); // меньше индекс - выше
                        } );
                    }

                    // пробуем получить термины, в порядке приоритета такс
                    foreach( $taxonomies as $taxname ){
                        if( $terms = get_the_terms( $post->ID, $taxname ) ){
                            // проверим приоритетные термины для таксы
                            $prior_terms = & $arg->priority_terms[ $taxname ];
                            if( $prior_terms && count($terms) > 2 ){
                                foreach( (array) $prior_terms as $term_id ){
                                    $filter_field = is_numeric($term_id) ? 'term_id' : 'slug';
                                    $_terms = wp_list_filter( $terms, array($filter_field=>$term_id) );

                                    if( $_terms ){
                                        $term = array_shift( $_terms );
                                        break;
                                    }
                                }
                            }
                            else
                                $term = array_shift( $terms );
                            break;
                        }
                    }
                }

                if( isset($save_post) ) $post = $save_post; // вернем обратно (для вложений)
            }

            // вывод

            // все виды записей с терминами или термины
            if( $term && isset($term->term_id) ){
                $term = apply_filters('kama_breadcrumbs_term', $term );

                // attachment
                if( is_attachment() ){
                    if( ! $post->post_parent )
                        $out = sprintf( $loc->attachment, esc_html($post->post_title) );
                    else {
                        if( ! $out = apply_filters('attachment_tax_crumbs', '', $term, $this ) ){
                            $_crumbs    = $this->_tax_crumbs( $term, 'self' );
                            $parent_tit = sprintf( $linkpatt, get_permalink($post->post_parent), get_the_title($post->post_parent) );
                            $_out = implode( $sep, array($_crumbs, $parent_tit) );
                            $out = $this->_add_title( $_out, $post );
                        }
                    }
                }
                // single
                elseif( is_single() ){
                    if( ! $out = apply_filters('post_tax_crumbs', '', $term, $this ) ){
                        $_crumbs = $this->_tax_crumbs( $term, 'self' );
                        $out = $this->_add_title( $_crumbs, $post );
                    }
                }
                // не древовидная такса (метки)
                elseif( ! is_taxonomy_hierarchical($term->taxonomy) ){
                    // метка
                    if( is_tag() )
                        $out = $this->_add_title('', $term, sprintf( $loc->tag, esc_html($term->name) ) );
                    // такса
                    elseif( is_tax() ){
                        $post_label = $ptype->labels->name;
                        $tax_label = $GLOBALS['wp_taxonomies'][ $term->taxonomy ]->labels->name;
                        $out = $this->_add_title('', $term, sprintf( $loc->tax_tag, $post_label,$tax_label, esc_html($term->name) ) );
                    }
                }
                // древовидная такса (рибрики)
                else {
                    if( ! $out = apply_filters('term_tax_crumbs', '', $term, $this ) ){
                        $_crumbs = $this->_tax_crumbs( $term, 'parent' );
                        $out = $this->_add_title( $_crumbs, $term, esc_html($term->name) );
                    }
                }
            }
            // влоежния от записи без терминов
            elseif( is_attachment() ){
                $parent = get_post($post->post_parent);
                $parent_link = sprintf( $linkpatt, get_permalink($parent), esc_html($parent->post_title) );
                $_out = $parent_link;

                // вложение от записи древовидного типа записи
                if( is_post_type_hierarchical($parent->post_type) ){
                    $parent_crumbs = $this->_page_crumbs($parent);
                    $_out = implode( $sep, array( $parent_crumbs, $parent_link ) );
                }

                $out = $this->_add_title( $_out, $post );
            }
            // записи без терминов
            elseif( is_singular() ){
                $out = $this->_add_title( '', $post );
            }
        }

        // замена ссылки на архивную страницу для типа записи
        $home_after = apply_filters('kama_breadcrumbs_home_after', '', $linkpatt, $sep, $ptype );

        if( '' === $home_after ){
            // Ссылка на архивную страницу типа записи для: отдельных страниц этого типа; архивов этого типа; таксономий связанных с этим типом.
            if( $ptype && $ptype->has_archive && ! in_array( $ptype->name, array('post','page','attachment') )
                && ( is_post_type_archive() || is_singular() || (is_tax() && in_array($term->taxonomy, $ptype->taxonomies)) )
            ){
                $pt_title = $ptype->labels->name;

                // первая страница архива типа записи
                if( is_post_type_archive())
                    $home_after = sprintf($arg->title_patt,$pt_title);
                // singular, paged post_type_archive, tax
                else{
                    $home_after = sprintf( $linkpatt, get_post_type_archive_link($ptype->name), $pt_title );

                    $home_after .= ( ($paged_num && ! is_tax()) ? $pg_end : $sep ); // пагинация
                }
            }
        }

        $before_out = sprintf( $linkpatt, home_url(), $loc->home ) . ( $home_after ? $sep.$home_after : ($out ? $sep : '') );
        $out = apply_filters('kama_breadcrumbs_pre_out', $out, $sep, $loc, $arg );
        $out = sprintf( $wrappatt, $before_out . $out );
        return apply_filters('kama_breadcrumbs', $out, $sep, $loc, $arg );
    }

    function _page_crumbs( $post ){
        $parent = $post->post_parent;

        $crumbs = array();
        while( $parent ){
            $page = get_post( $parent );
            $crumbs[] = sprintf( $this->arg->linkpatt, get_permalink($page), esc_html($page->post_title) );
            $parent = $page->post_parent;
        }

        return implode( $this->arg->sep, array_reverse($crumbs) );
    }

    function _tax_crumbs( $term, $start_from = 'self' ){
        $termlinks = array();
        $term_id = ($start_from === 'parent') ? $term->parent : $term->term_id;
        while( $term_id ){
            $term       = get_term( $term_id, $term->taxonomy );
            $termlinks[] = sprintf( $this->arg->linkpatt, get_term_link($term), esc_html($term->name) );
            $term_id    = $term->parent;
        }

        if( $termlinks )
            return implode( $this->arg->sep, array_reverse($termlinks) ) /*. $this->arg->sep*/;
        return '';
    }

    // добалвяет заголовок к переданному тексту, с учетом всех опций. Добавляет разделитель в начало, если надо.
    function _add_title( $add_to, $obj, $term_title = '' ){
        $arg = & $this->arg; // упростим...
        $title = $term_title ? $term_title : esc_html($obj->post_title); // $term_title чиститься отдельно, теги моугт быть...
        $show_title = $term_title ? $arg->show_term_title : $arg->show_post_title;

        // пагинация
        if( $arg->pg_end ){
            $link = $term_title ? get_term_link($obj) : get_permalink($obj);
            $add_to .= ($add_to ? $arg->sep : '') . sprintf( $arg->linkpatt,$link,$title );
        }
        // дополняем - ставим sep
        elseif( $add_to ){
            if( $show_title )
                $add_to .= $arg->sep . sprintf( $arg->title_patt, $title );
            elseif( $arg->last_sep )
                $add_to .= $arg->sep;
        }
        // sep будет потом...
        elseif( $show_title )
            $add_to = sprintf( $arg->title_patt,$title );

        return $add_to;
    }

}
//Конец хлебных крошек

// Удаляем H2 из шаблона пагинации
add_filter('navigation_markup_template', 'dental_nav_template', 10, 2 );
function dental_nav_template( $template, $class ){
    return '
	<nav class="navigation %1$s" role="navigation">
		<div class="nav-links">%3$s</div>
	</nav>    
	';
}
//Изменяем запрос для пагинации на странице мероприятий
add_action( 'pre_get_posts', 'custom_archive_for_date' );
function custom_archive_for_date( $query ) {
    if($query->get('post_type')=='events'&&!$query->get('order')&&!is_admin()&&!$query->is_single()){
        $today = date('y-m-d');
        $meta_query = array(
            array(
                'key' => 'date_events',
                'value' => $today,
                'type' => 'DATE',
                'compare' => '<'
            )
        );
        $query->set('order','DESC');
        $query->set('orderby','meta_value');
        $query->set('meta_key','date_events');
        $query->set('meta_query',array($meta_query));
    }
}
?>