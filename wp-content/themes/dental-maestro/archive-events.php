<?php get_header(); ?>
<!-- Begin wrapper -->
<div class="wrapper">
    <?php get_template_part('templates/breadcrumbs');?>
    <?php get_template_part('templates/slider_events');?>
<div class="container-fluid">
    <h1 class="title-bordered">Архив мероприятий</h1>

    <!-- Begin categories -->
    <div class="categories-wrap">
        <?php
         while (have_posts()) {the_post();?>
        <!-- Begin single category -->
        <div class="category">
            <!-- Begin single course -->
            <div class="course course--big">

                <div class="course__inner" style="background-image: url(<?php echo get_field('photo_event');?>);">
                    <div class="main-slider__date"><?php echo apply_filters('rus_date',get_field('date_events'));?></div>
                    <h3 class="course__title"><?php the_title();?></h3>
                    <a href="<?php the_permalink();?>" class="button button-angle button-angle--small">Подробнее</a>
                </div>
                <div class="course__in-hover course__in-hover--long">
                    <div class="main-slider__date"><?php echo apply_filters('rus_date',get_field('date_events'));?></div>
                    <h3 class="course__title course__title--in-hover"><?php the_title();?></h3>
                    <div class="course__description">
                       <?php echo apply_filters('clip_string',get_field('description'),300);?>
                    </div>
                    <a href="<?php the_permalink();?>" class="button button-angle button-angle--small course__button">Подробнее</a>
                </div>

            </div>
            <!-- End single course -->
        </div>
        <!-- End single category -->
        <?php } ?>
    </div>
    <!-- End categories -->
    <!-- Begin pagination -->
    <nav class="navigation pagination">
        <?php
        $args = array(
            'show_all'     => false, // показаны все страницы участвующие в пагинации
            'end_size'     => 1,     // количество страниц на концах
            'mid_size'     => 2,     // количество страниц вокруг текущей
            'prev_next'    => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
            'prev_text'    => __('Раньше'),
            'next_text'    => __('Позже'),
            'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
            'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
            'screen_reader_text' => __( 'Posts navigation' ),
        );

        the_posts_pagination($args);
        // ?>
    </nav>
    <!-- End pagination -->

</div>

</div>
<!-- End wrapper -->
<?php get_footer(); ?>
