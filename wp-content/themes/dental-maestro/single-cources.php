<?php get_header(); ?>
    <!-- Begin wrapper -->
    <div class="wrapper">

        <?php get_template_part('templates/breadcrumbs');?>

        <div class="container-fluid">
            <h1 class="title-bordered">
                <span><?php echo get_field('full_title'); ?></span>
            </h1>
            <!-- Begin course programm -->
            <div class="programm">
                <div class="programm__description">
                    <?php echo get_field('full_desc'); ?>
                </div>
                <div class="programm__lecturers">
                    <h3 class="programm__title">Лекторы</h3>
                    <!-- Begin lecturers slider -->
                    <div class="lecturers-slider">
                        <div class="swiper-container lecturers-slider__container">

                            <div class="swiper-wrapper">
                                <div class="swiper-slide lecturers-slider__slide">
                                    <!-- Begin lecturer -->
                                    <?php
//                                    Выводим список лекторов из кастомного поля
                                    $lektors = get_field('lektors');
                                    foreach ($lektors as $lektor) {
                                        ?>
                                        <div class="lecturer lecturers-slider__lecturer img-fit">
                                            <img src="<?php echo get_field('lektor_photo',$lektor->ID) ?>" alt="<?php echo $lektor->lektor_name; ?>">
                                            <div class="lecturer__in">
                                                <h4 class="lecturer__title"><?php echo $lektor->post_title; ?></h4>
                                                <p class="lecturer__description"><?php echo apply_filters('clip_string',$lektor->lektor_description,100); ?></p>
                                                <p style="margin: 0"><a class="button button-angle button-angle-cust" href="<?php echo get_post_permalink( $lektor->ID, false, false );?>">о лекторе</a></p>
                                            </div>
                                        </div>
                                    <?php }; ?>
                                    <!-- End lecturer -->
                                </div>
                            </div>

                            <div class="swiper-button-prev lecturers-slider__prev"></div>
                            <div class="swiper-button-next lecturers-slider__next"></div>

                        </div>
                    </div>
                    <!-- End lecturers slider -->
                    <!-- Begin programm info -->
                    <div class="programm__info">
                        <div class="programm__time">Длительность:
                            <b><?php echo get_field('duration'); ?> <?php echo apply_filters('count_days', get_field('duration')); ?></b>
                        </div>
                        <?php if (get_field('number_listeners') != 'индивидуальный') { ?>
                            <div class="programm__members">Участники:
                                <b><?php echo get_field('number_listeners'); ?></b></div>
                        <?php } else {
                        } ?>
                    </div>
                    <!-- End programm info -->
                </div>
            </div>
            <!-- End course programm -->

            <div class="align-center">
                <a class="button button-angle button-angle--big js-button-form">Записаться на курс</a>

                <div class="form hidden">
                    <h3>Подать заявку на участие</h3>
                    <?php echo do_shortcode("[contact-form-7 id=\"172\" title=\"form-courses\"]"); ?>
                    <div class="form__close"></div>
                </div>
            </div>
            <h2 class="title-bordered">Именной сертификат</h2>
            <img src="<?php bloginfo('template_url'); ?>/img-compress/pic/sertificate.jpg" alt="" class="alignleft">

            <?php echo get_field('adv_desc'); ?>
        </div>
    </div>
    <!-- End wrapper -->

<?php get_footer(); ?>