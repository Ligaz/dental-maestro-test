<?php get_header() ?>
<!-- Begin wrapper -->
<div class="wrapper">
    <?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );?>
    <?php get_template_part('templates/breadcrumbs');?>
    <div class="container-fluid">
        <h1 class="title-bordered"><?php echo get_field('direct_title','directions'.'_'.$term->term_id);?></h1>

        <!-- Begin categories -->
        <div class="categories-wrap">
            <?php while (have_posts()) {the_post();?>
            <!-- Begin single category -->
            <div class="category">
                <!-- Begin single course -->
                <div class="course course--big">

                    <div class="course__inner" style="background-image: url(<?php echo get_field('course_image'); ?>);">
                        <div class="course__time-block">
                            <span class="course__time"><?php echo get_field('duration'); ?></span>
                        </div>
                        <div class="course__kind" style="background-image: url(<?php $base_url=bloginfo('template_url'); echo get_field('number_listeners')=='индивидуальный'? $base_url.'/img/icons/man.png':$base_url.'/img/icons/group.png'?>);">
                        </div>
                        <div class="course__ico">
                            <img src="<?php echo get_field('icon','directions'.'_'.$term->term_id);?>" alt="">
                        </div>
                        <h3 class="course__title"><?php the_title(); ?></h3>
                        <a href="course.html" class="button button-angle button-angle--small">Подробнее</a>
                    </div>

                    <div class="course__in-hover course__in-hover--long">
                        <div class="course__time-block">
                            Курс длится <span class="course__time"><?php echo get_field('duration'); ?> <?php echo apply_filters('count_days', get_field('duration')); ?></span>
                        </div>

                        <div class="course__kind" style="background-image: url(<?php $base_url=bloginfo('template_url'); echo get_field('number_listeners')=='индивидуальный'? $base_url.'/img/icons/man.png':$base_url.'/img/icons/group.png'?>)">
                            <?php echo get_field('number_listeners'); ?>
                        </div>
                        <div class="course__ico course__ico--in-hover">
                            <img src="<?php echo get_field('icon','directions'.'_'.$term->term_id);?>" alt="">
                        </div>
                        <h3 class="course__title course__title--in-hover"><?php the_title(); ?></h3>
                        <div class="course__description">
                            <?php echo get_field('short_desc');?>
                        </div>
                        <a href="<?php the_permalink();?>" class="button button-angle button-angle--small course__button">Подробнее</a>
                    </div>

                </div>
                <!-- End single course -->
            </div>
            <!-- End single category -->
            <?php } ?>

        </div>
        <!-- End categories -->
    `<nav class="navigation pagination">
            <?php
            $args = array(
                'show_all'     => false, // показаны все страницы участвующие в пагинации
                'end_size'     => 1,     // количество страниц на концах
                'mid_size'     => 2,     // количество страниц вокруг текущей
                'prev_next'    => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
                'prev_text'    => __('Раньше'),
                'next_text'    => __('Позже'),
                'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
                'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
                'screen_reader_text' => __( 'Posts navigation' ),
            );

            the_posts_pagination($args);

            // ?>
        </nav>
    </div>

</div>
<!-- End wrapper -->
<?php get_footer();?>
