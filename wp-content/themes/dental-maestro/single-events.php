<?php get_header(); ?>
    <!-- Begin wrapper -->
    <div class="wrapper">

        <?php get_template_part('templates/breadcrumbs');?>

        <div class="container-fluid">
            <h1 class="title-bordered">
                <span><?php echo the_title(); ?></span>
            </h1>
            <!-- Begin course programm -->
            <div class="programm programm_events">
                <div class="programm__description">
                    <?php echo get_field('description'); ?>
                </div>
                    <!-- Begin programm info -->
                    <div class="programm__info">
                        <div class="programm__time">Дата:
                            <b><?php echo get_field('duration'); ?><?php echo apply_filters('rus_date', get_field('date_events')); ?></b>
                        </div>
                    </div>
                    <!-- End programm info -->
                </div>
            </div>
            <!-- End course programm -->
         <?php
        $date_now= date('Y-m-d');
        $date_event= get_field('date_events');
        if(strtotime($date_event)>=strtotime($date_now)){?>
            <div class="align-center">
                <a class="button button-angle button-angle--big js-button-form">Записаться на мероприятие</a>
                <div class="form hidden">
                    <h3>Подать заявку на участие</h3>
                        <?php echo do_shortcode("[contact-form-7 id=\"171\" title=\"form-events\"]"); ?>
                    <div class="form__close"></div>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
    <!-- End wrapper -->

<?php get_footer(); ?>