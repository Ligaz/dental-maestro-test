<!-- Begin lectors section -->
<section>
    <div class="container-fluid">
        <h2 class="title-bordered">Наши лекторы</h2>
        <div class="main-lecturers">
<!--            Достаем всех лекторов для вывода на главной странице-->
            <?php
            $args = array(
                'post_type' => 'lektors',
                'publish' => true
            );
            $query = new WP_Query( $args );
            while ( $query->have_posts() ) { $query->the_post();
                ?>
                <!-- Begin lecturer -->
                <div class="lecturer img-fit">
                    <img src="<?php echo get_field('lektor_photo')?>" alt="">
                    <div class="lecturer__in">
                        <h4 class="lecturer__title"><?php the_title();?></h4>
                        <p class="lecturer__description"><?php echo apply_filters('clip_string',get_field('lektor_description'),100); ?></p>
                        <p><a class="button button-angle button-angle-cust" href="<?php the_permalink();?>">О лекторе</a></p>
                    </div>
                </div>
                <!--End lecturer-->
            <?php }?>

        </div>
    </div>
</section>
<!-- End lectors section -->