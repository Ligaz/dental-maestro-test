<!-- Begin courses -->
<section class="container-fluid">
    <h2 class="title-bordered">Наши курсы</h2>
    <div class="main-courses">
        <!-- Begin single course -->
        <?php
        //Достаем таксономию и проганяем в цикле
        $taxonomies = get_terms('directions','orderby=name&hide_empty=0' );
        foreach ($taxonomies as $taxonomy){ ?>
            <div class="course">
                <div class="course__inner" style="background-image: url(<?php echo get_field('back_image','directions'.'_'.$taxonomy->term_id); ?>">
                    <div class="course__ico">
                        <img src="<?php echo get_field('icon','directions'.'_'.$taxonomy->term_id);?>" alt="">
                    </div>
                    <h3 class="course__title"><?php echo $taxonomy->name; ?></h3>
                </div>
                <div class="course__in-hover">
                    <div class="course__ico course__ico--in-hover">
                        <img src="<?php echo get_field('icon','directions'.'_'.$taxonomy->term_id);?>" alt="">
                    </div>
                    <h3 class="course__title course__title--in-hover"><?php echo $taxonomy->name; ?></h3>
                    <div class="course__description">
                        <?php echo apply_filters('clip_string',$taxonomy->description,500); ?>
                    </div>
                    <a href="<?php echo get_term_link($taxonomy->term_id, 'directions')?>" class="button button-angle">Перейти</a>
                </div>
            </div>
        <?php } ?>
        <!-- End single course -->
    </div>
</section>
<!-- End courses -->