<!-- Begin slider block -->
<?php
//Создаем запрос для выборки только тех мероприятий, которые ещё не прошли и сортируем их от поздней до ранней
$today = date('y-m-d');
$meta_query = array(
    array(
        'key' => 'date_events',
        'value' => $today,
        'type' => 'DATE',
        'compare' => '>='
    )
);
$args = array(
    'post_type' => 'events',
    'order' => 'ASC',
    'orderby' => 'meta_value',
    'meta_key' => 'date_events',
    'meta_query' => $meta_query
);
$query = new WP_Query( $args ); ?>
<?php
if($query->have_posts()){
    ?>
<section class="main-slider">
    <div class="container-fluid">
        <div class="main-slider__in-wrap">
            <div class="main-slider__in">
                <div class="swiper-container main-slider__container">
                    <div class="swiper-wrapper">
                        <?php while ( $query->have_posts() ) { $query->the_post();
                        ?>
                            <div class="swiper-slide">
                                <div class="main-slider__date"><?php echo apply_filters('rus_date',get_field('date_events'));?></div>
                                <?php
                                //Проверяем, если дата мероприятия совпадает с текущей, добавляем блок, которые об этом уведомляет
                                $date_now= date('Y-m-d');
                                $date_event= get_field('date_events');
                                if(strtotime($date_event)==strtotime($date_now)){?>
                                <div class="main-slider__top">Сегодня состоится встреча:</div>
                                <?php } ?>
                                <div class="main-slider__bottom">"<?php the_title(); ?>"</div>
                                <a href="<?php the_permalink();?>">Пойти</a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="swiper-button-prev main-slider__prev"></div>
                    <div class="swiper-button-next main-slider__next"></div>
                </div>
                <div class="swiper-pagination main-slider__pagination"></div>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<!-- End slider block -->