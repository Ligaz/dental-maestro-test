<!-- Begin partners section -->
<section>
    <div class="container-fluid">
        <h2 class="title-bordered">Партнёры</h2>
        <div class="partners">
            <?php
            $args = array(
                'post_type' => 'partners',
                'publish' => true
            );
            $query = new WP_Query( $args );
            while ( $query->have_posts() ) { $query->the_post();
                ?>
                <a href="<?php echo get_field('url');?>" target="_blank">
                    <img src="<?php echo get_field('img_photo');?>" alt="<?php the_title()?>">
                </a>
            <?php } ?>
        </div>
    </div>
</section>
<!-- End partners section -->