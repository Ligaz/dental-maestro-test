<!-- Begin privileges section -->
<section>
    <div class="container-fluid">
        <h2 class="title-bordered">Наши преимущества</h2>
    </div>
</section>

<!-- Begin styling privileges -->
<style>
    .main-privileges-images__img--1 {
        background-image: url(<?php bloginfo('template_url')?>/img/pic/cup.png);
    }
    .main-privileges-images__img--2 {
        background-image: url(<?php bloginfo('template_url')?>/img/pic/books.png);
    }
    .main-privileges-images__img--3 {
        background-image: url(<?php bloginfo('template_url')?>/img/pic/mortarboard.png);
    }
    .main-privileges-images__img--4 {
        background-image: url(<?php bloginfo('template_url')?>/img/pic/teeth.png);
    }

    @media (max-width: 991px) {
        .main-privileges-images__img--1 {
            background-image: url(<?php bloginfo('template_url')?>/img/pic/cup-hover.png);
        }
        .main-privileges-images__img--2 {
            background-image: url(<?php bloginfo('template_url')?>/img/pic/books-hover.png);
        }
        .main-privileges-images__img--3 {
            background-image: url(<?php bloginfo('template_url')?>/img/pic/mortarboard-hover.png);
        }
        .main-privileges-images__img--4 {
            background-image: url(<?php bloginfo('template_url')?>/img/pic/teeth-hover.png);
        }
    }

    .main-privileges-images__inner:hover .main-privileges-images__img--1 {
        background-image: url(<?php bloginfo('template_url')?>/img/pic/cup-hover.png);
    }
    .main-privileges-images__inner:hover .main-privileges-images__img--2 {
        background-image: url(<?php bloginfo('template_url')?>/img/pic/books-hover.png);
    }
    .main-privileges-images__inner:hover .main-privileges-images__img--3 {
        background-image: url(<?php bloginfo('template_url')?>/img/pic/mortarboard-hover.png);
    }
    .main-privileges-images__inner:hover .main-privileges-images__img--4 {
        background-image: url(<?php bloginfo('template_url')?>/img/pic/teeth-hover.png);
    }
</style>
<!-- End styling privileges -->
<div class="main-privileges">
    <div class="container-fluid">
        <div class="main-privileges-images">
            <div class="main-privileges-images__inner">
                <div class="main-privileges-images__img main-privileges-images__img--1">
                    <div class="main-privileges-images__txt main-privileges-images__txt--white">Более <span class="main-privileges-images__txt--bold-b">10 лет</span> опыта</div>
                </div>
            </div>
            <div class="main-privileges-images__inner">
                <div class="main-privileges-images__img main-privileges-images__img--2">
                    <div class="main-privileges-images__txt">Десятки курсов</div>
                </div>
            </div>
            <div class="main-privileges-images__inner">
                <div class="main-privileges-images__img main-privileges-images__img--3">
                    <div class="main-privileges-images__txt">Сотни выпускников</div>
                </div>
            </div>
            <div class="main-privileges-images__inner">
                <div class="main-privileges-images__img main-privileges-images__img--4">
                    <div class="main-privileges-images__txt">Тысячи вылеченных зубов</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End privileges section -->