<?php get_header(); ?>
<!-- Begin wrapper -->
<div class="wrapper">
    <?php get_template_part('templates/breadcrumbs');?>
    <div class="container-fluid">
        <h1 class="title-bordered">Альбомы</h1>

        <!-- Begin categories -->
        <div class="categories-wrap">
            <?php
            $taxonomies = get_terms('photogallery','orderby=name&hide_empty=0' );
            foreach ($taxonomies as $taxonomy){ ?>
                <!-- Begin single category -->
                <div class="category">
                    <!-- Begin single course -->
                    <div class="course course--big">

                        <div class="course__inner" style="background-image: url(<?php echo get_field('gal_img','photogallery'.'_'.$taxonomy->term_id); ?>);">
                            <h3 class="course__title"><?php echo $taxonomy->name; ?></h3>
                            <a href="<?php echo get_term_link($taxonomy->term_id, 'photogallery');?>" class="button button-angle button-angle--small">Просмотреть</a>
                        </div>
                        <div class="course__in-hover course__in-hover--long">
                            <h3 class="course__title course__title--in-hover"><?php echo $taxonomy->name; ?></h3>
                            <div class="course__description">
                                <?php echo apply_filters('clip_string',$taxonomy->description,300);?>
                            </div>
                            <a href="<?php echo get_term_link($taxonomy->term_id, 'photogallery');?>" class="button button-angle button-angle--small course__button">Просмотреть</a>
                        </div>

                    </div>
                    <!-- End single course -->
                </div>
                <!-- End single category -->
            <?php } ?>
        </div>
        <!-- End categories -->

    </div>

</div>

</div>
<!-- End wrapper -->
<?php get_footer(); ?>

