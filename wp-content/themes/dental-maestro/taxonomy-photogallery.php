<?php get_header(); ?>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );?>
        <?php get_template_part('templates/breadcrumbs');?>
        <div class="container-fluid">
            <h1 class="title-bordered"><?php echo $term->name;?></h1>
        </div>
        <!-- Begin photo gallery -->
        <div class="photo-slider">
            <!-- Begin photo slider -->
            <div class="swiper-container photo-slider__container">
                <div class="swiper-wrapper photo-slider__wrapper">
                    <?php while (have_posts()) {the_post();
                    ?>
                    <div class="swiper-slide">
                        <div class="photo-slider__slide">
                            <img src="<?php echo get_field('image');?>" alt="<?php the_title();?>">
                            <div class="photo-slider__description"><?php echo get_field('description');?></div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="swiper-button-prev photo-slider__prev"></div>
                <div class="swiper-button-next photo-slider__next"></div>
            </div>
            <!-- End photo slider -->
            <!-- Begin thumbnails -->
            <div class="photo-slider__thumbnails">
                <div class="container-fluid" style="position: relative;">
                    <div class="swiper-container photo-slider__thumbnails-container">
                        <div class="swiper-wrapper photo-slider__thumbnails-wrapper">
                            <?php while (have_posts()) {the_post();?>
                            <div class="swiper-slide">
                                <img src="<?php echo get_field('image');?>" alt="<?php the_title();?>">
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="swiper-button-prev photo-slider__thumbnails-prev"></div>
                    <div class="swiper-button-next photo-slider__thumbnails-next"></div>
                </div>
            </div>
            <!-- End thumbnails -->
        </div>
        <!-- End photo gallery -->

    </div>
    <!-- End wrapper -->
<?php get_footer(); ?>