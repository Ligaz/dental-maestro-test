<?php get_header(); ?>
    <!-- Begin wrapper -->
    <div class="wrapper">

        <?php get_template_part('templates/breadcrumbs');?>

        <div class="container-fluid">
            <!-- Begin lecturer info block -->
            <?php while (have_posts()) {the_post();?>
                <div class="lecturer-info">
                    <div class="lecturer-info__title">
                        <?php echo get_field('lektor_name'); ?>
                    </div>
                    <div class="lecturer-info__photo img-fit">
                        <img src="<?php echo get_field('lektor_photo')?>" >
                    </div>
                    <div class="lecturer-info__description">
                        <?php echo apply_filters('clip_string',get_field('lektor_description'),500); ?>
                        <p><a class="button button-angle button-angle-cust" href="<?php the_permalink();?>">Просмотреть профиль лектора</a></p>
                    </div>

                </div>
            <?php } ?>
            <!-- End lecturer info block -->
        </div>
        <?php the_posts_pagination(); ?>
    </div>
    <!-- End wrapper -->
<?php get_footer(); ?>