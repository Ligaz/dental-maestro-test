<!-- Begin footer -->
<footer class="footer">
    <div class="container-fluid padding-0">
        <div class="footer__in">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer__logo">
                        <a href="/">
                            <img src="<?php bloginfo('template_url')?>/img/logo.png" alt="Дентал-маэстро">
                        </a>
                        <div class="tagline">Учебный центр стоматологии</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer__contacts">
                        <h4 class="footer__title">Контакты:</h4>
                        <a class="footer__link" href="tel:+38 (061) 233-50-31">+38 (061) 233-50-31</a>
                        <a class="footer__link" href="tel:+38 (061) 233-50-31">+38 (061) 233-50-31</a>
                        <address class="footer__address">г. Запорожье, ул. Пушкина, д. 45</address>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer__info">
                        <div class="social">
                            <a class="social__link" href="#"><i class="fa fa-vk"></i></a>
                            <a class="social__link" href="#"><i class="fa fa-facebook"></i></a>
                            <a class="social__link" href="#"><i class="fa fa-google-plus"></i></a>
                            <a class="social__link" href="#"><i class="fa fa-youtube"></i></a>
                        </div>
                        <div class="copyright">2016 (c) ДенталМаэстро</div>
                        <div class="creator">Сайт разработан веб-студией <a href="https://web-shturman.com.ua">«Штурман»</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End footer -->

<div class="hidden"></div>

<!-- Load CSS -->
<script>
    var ms=document.createElement("link");ms.rel="stylesheet";ms.href="<?php bloginfo('template_url')?>/libs/components-font-awesome/css/font-awesome.min.css";document.getElementsByTagName("head")[0].appendChild(ms);

    var ms=document.createElement("link");ms.rel="stylesheet";ms.href="<?php bloginfo('template_url')?>/style.css";document.getElementsByTagName("head")[0].appendChild(ms);
</script>

<!-- Load Scripts -->
<script>var scr = {"scripts":[
        {"src" : "<?php bloginfo('template_url')?>/libs/object-fit-images-master/dist/ofi.min.js", "async" : false},
        {"src" : "<?php bloginfo('template_url')?>/libs/swiper/dist/js/swiper.min.js", "async" : false},
        {"src" : "<?php bloginfo('template_url')?>/js/es5/common.js", "async" : false}
    ]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
</script>
</body>
</html>