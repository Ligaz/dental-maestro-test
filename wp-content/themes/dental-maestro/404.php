<?php get_header(); ?>
<div class="wrapper">
    <div class="error_wrap">
    <div class="error_404">
        404
    </div>
    <div class="error_description">
        Запрашиваемая Вами страница не найдена!!!
    </div>
    <p><a class="button button-angle button-angle--small" href="<?php bloginfo('url');?>">Вернутся на главную</a></p>
    </div>
</div>
<?php get_footer(); ?>